﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;

namespace GameScreen
{
    public partial class Form1 : Form
    {
        Button forfiet;
        TextBox PScore, OScore;
        int Player = 0, Opponent = 0, i = 0;
        PictureBox pb;
        Button[] Cards = new Button[11];
        char[] shapes = new char[4];
        int[] used = new int[52];
        Random rnd = new Random();
        TcpClient server;
        string message = null;
        NetworkStream serverStream;
        bool sent = false;

        public Form1()
        {
            InitializeComponent();
            GenerateRest();
            this.FormClosed += new FormClosedEventHandler(Form1_FormClosed); //closed form handler

            //Establish connection with the server
            try
            {
                server = new TcpClient("127.0.0.1", 8820);
                Thread t = new Thread(WaitForMessage);
                t.Start();
            }
            catch (SocketException e)
            {
                //If server connection fails, close program and show message
                MessageBox.Show(e.ToString(), "Connection error");
                this.Close();
            }
        }

        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            MessageBox.Show(PScore.Text + "\n" + OScore.Text, "Score");
            message = "2000";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void forfietClick(object sender, Button e)
        {
            this.Close();
        }

        private void btnClick(object sender, Button e)
        {
            bool equal = false;
            int type, shape;
            byte[] buffer = new byte[4];
            Button pb = (Button)sender;
            // use picutreBox

            //reset current selected card
            Cards[i].Enabled = true;
            Cards[i].BackgroundImage = Image.FromFile("PNG-cards\\card_back_red.png");

            //reset opponent card
            this.pb.ImageLocation = "PNG-cards\\card_back_blue.png";
            for (int j = 0; j < 10; j++)
            {
                if(Cards[j].Equals(pb))
                {
                    equal = true;
                    i = j;
                }
                Cards[j].Enabled = true;
            }

            if(equal && message == null)
            {
                //generate random card and make it used
                type = rnd.Next(12) + 1;
                shape = rnd.Next(3);
                while (used[type * (shape + 1)] == 1)
                {
                    type = rnd.Next(12) + 1;
                    shape = rnd.Next(3);
                }
                used[type * (shape + 1)]++;

                //write message
                message = "1";
                if (type < 10)
                {
                    message += '0' + type.ToString();
                }
                else
                {
                    message += type.ToString();
                }
                message += shapes[shape];

                Cards[i].BackgroundImage = Image.FromFile(handlePath(message));
                Cards[i].Enabled = false; //disable click

                //send message
                buffer = new ASCIIEncoding().GetBytes(message);
                serverStream.Write(buffer, 0, 4);
                serverStream.Flush();

                sent = true;
            }
        }

        private void GenerateCards()
        {
            for (int i = 0; i < 10; i++)
            {
                Cards[i] = new Button();
                Cards[i].Size = new System.Drawing.Size(80, 100);
                Cards[i].BackgroundImage = Image.FromFile("PNG-cards\\card_back_red.png");
                Cards[i].BackgroundImageLayout = ImageLayout.Stretch;
                Cards[i].Location = new Point((i + 1) * 90, this.Height - 170);
                Cards[i].Name = i.ToString();
                Cards[i].Click += (sender1, ex) => this.btnClick(sender1, Cards[i]);
                this.Controls.Add(Cards[i]);
            }
        }

        private void GenerateRest()
        {
            //initialize shapes array
            shapes[0] = 'H';
            shapes[1] = 'C';
            shapes[2] = 'S';
            shapes[3] = 'D';

            //initialize array to check if card is used
            for(int i = 0; i < 52; i++)
            {
                used[i] = 0;
            }

            //Opponent's card
            pb = new PictureBox(); 
            pb.Size = new System.Drawing.Size(80, 100);
            pb.ImageLocation = "PNG-cards\\card_back_blue.png";
            pb.SizeMode = PictureBoxSizeMode.StretchImage;
            pb.Location = new Point(500, this.Height - 370);
            this.Controls.Add(pb);

            //forfiet button
            forfiet = new Button();
            forfiet.Text = "Forfiet";
            forfiet.Size = new System.Drawing.Size(90, 25);
            forfiet.Location = new Point(495, this.Height - 430);
            forfiet.BackColor = Color.White;
            forfiet.Enabled = false;
            forfiet.Click += (sender1, ex) => this.forfietClick(sender1, forfiet);
            this.Controls.Add(forfiet);

            //Player's score textbox
            PScore = new TextBox();
            PScore.Text = "Your Score: 0";
            PScore.ReadOnly = true;
            PScore.Location = new Point(90, this.Height - 430);
            PScore.BackColor = Color.Green;
            PScore.BorderStyle = BorderStyle.None;
            this.Controls.Add(PScore);

            //Opponent's score textbox
            OScore = new TextBox();
            OScore.Text = "Opponent Score: 0";
            OScore.ReadOnly = true;
            OScore.Location = new Point(900, this.Height - 430);
            OScore.BackColor = Color.Green;
            OScore.BorderStyle = BorderStyle.None;
            this.Controls.Add(OScore);
        }

        private void WaitForMessage()
        {
            try
            {
                string input = "";
                int bytesRead = 0;
                byte[] bufferIn = new byte[4], buffer = new byte[4];
                
                while (true)
                {
                    serverStream = server.GetStream();
                    
                    if(serverStream.CanRead)
                    {
                        bufferIn = new byte[4];
                        bytesRead = serverStream.Read(bufferIn, 0, 4);
                        input = new ASCIIEncoding().GetString(bufferIn);
                    }

                    if (message != null)
                    {
                        if (message[0] == '2')
                        {
                            //send finish game message
                            buffer = new ASCIIEncoding().GetBytes(message);
                            serverStream.Write(buffer, 0, 4);
                            serverStream.Flush();
                            break; //stop thread
                        }                       
                    }

                    if (input[0] == '0')
                    {
                        this.BeginInvoke((Action)delegate ()
                        {
                            forfiet.Enabled = true;
                            GenerateCards();
                        });
                    }

                    if(input[0] == '1')
                    {
                        while (!sent) ;

                        if(Int32.Parse(message.Substring(1,2)) > Int32.Parse(input.Substring(1, 2))) //which card is bigger
                        {
                            Player++;
                            Opponent--;
                        }
                        else if(Int32.Parse(message.Substring(1, 2)) < Int32.Parse(input.Substring(1, 2)))
                        {
                            Player--;
                            Opponent++;
                        }

                        this.BeginInvoke((Action)delegate ()
                        {
                            pb.ImageLocation = handlePath(input);
                            PScore.Text = "Your Score: " + Player;
                            OScore.Text = "Opponent Score: " + Opponent;
                        });

                        message = null;
                        sent = false;
                    }

                    if(input[0] == '2')
                    {
                        this.BeginInvoke((Action)delegate ()
                        {
                            this.Close();
                        });
                        break; //stop thread
                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Game cannot continue: \n" + e.ToString(), "Error");
            }
        }

        private string handlePath(string message)
        {
            string path = "PNG-cards\\";
            if(message[1] != '0')
            {
                if(message[2] == '1')
                {
                    path += "jack_";
                }
                else if (message[2] == '2')
                {
                    path += "queen_";
                }
                else if (message[2] == '3')
                {
                    path += "king_";
                }
                else
                {
                    path += "10_";
                }
            }
            else
            {
                if(message[2] == '1')
                {
                    path += "ace_";
                }
                else
                {
                    path += message[2] + "" + '_';
                }
            }

            if (message[3] == 'H')
            {
                path += "of_hearts.png";
            }
            else if (message[3] == 'C')
            {
                path += "of_clubs.png";
            }
            else if (message[3] == 'S')
            {
                path += "of_spades.png";
            }
            else
            {
                path += "of_diamonds.png";
            }
            return path;
        }


    }
}
